module.exports = {
  mode: "jit",
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "#319795",
        secondary: "#81E6D9",
        grey: "#718096",
      },
      fontSize: {
        "130px": "130px",
      },
    },
  },
  plugins: [],
};
